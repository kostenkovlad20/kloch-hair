

function tabNavClickHandler(e) {
    e.preventDefault();

    if(e.target.classList.contains('tab__link')){
        
        $('.tab__link.active').removeClass('active');
        e.target.classList.add('active');

        // $(`.tab__content[data-slide='${e.target}']`)
        $('.tab__content').removeClass('active');
        $(`[data-tab-id='${e.target.getAttribute('href')}']`).addClass('active');

    }


}


function bindEvents() {
    const tabNav = document.querySelector('.tab__nav');
    tabNav.addEventListener('click', tabNavClickHandler)
}


function init() {
    bindEvents()
}

export default {
    init:init
}
