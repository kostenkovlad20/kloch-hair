import tab from './tab';


tab.init();

$(".hamburger").click(function (e) {
  document.querySelector(".mobile").classList.add("active");
  document.querySelector("html").classList.add("oh");
  this.classList.add("active");
});

$(".layout").click(function (e) {
  document.querySelector(".mobile.active").classList.remove("active");
  document.querySelector("html").classList.remove("oh");
  document.querySelector(".hamburger").classList.remove("active");
});

$(".mobile__close").click(function (e) {
  document.querySelector(".mobile.active").classList.remove("active");
  document.querySelector("html").classList.remove("oh");
  document.querySelector(".hamburger").classList.remove("active");
});




