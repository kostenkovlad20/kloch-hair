$(document).ready(function () {
    if ($('.block-haircuts').find('.block-haircuts-image__item').length > 8) {
        $('#show-hide-haircut-button').on('click', function () {
            $('.block-haircuts .block-haircuts-image__item:nth-child(n+9)').slideToggle('');
            $(this).toggleClass('hide-more');
            if ($(this).hasClass('hide-more')) {
                $(this).html('Скрыть');

            } else {
                $(this).html('Показать еще');
            }
        })
    } else {
        $('#show-hide-haircut-button').hide();
    }

});