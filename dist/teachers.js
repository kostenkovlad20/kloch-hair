$(document).ready(function () {
    if ($('.quotes').find('li').length > 7) {
        $('#show-hide').on('click', function () {
            $('.quotes li:nth-child(n+8)').slideToggle('');
            $(this).toggleClass('hide-more');
            if ($(this).hasClass('hide-more')) {
                $(this).html('Скрыть');

            } else {
                $(this).html('Показать все');

            }
        })
    } else {
        $('#show-hide').hide();
    }


    $("[data-fancybox]").fancybox();

    $('.about-teachers__about').on('click', function () {

        var swiper = new Swiper('.swiper-container', {
            spaceBetween: 100,
            slidesPerView: '1',
            pagination: {
                el: '.swiper-pagination',
                type: 'fraction',
            },
            navigation: {
                nextEl: '.next-arrow',
                prevEl: '.prev-arrow',
            },
        });
    });

});



