$(document).ready(function () {
    if ($('.show-hide-list').find('li').length > 7) {
        $('#show-hide-button').on('click', function () {
            $('.show-hide-list li:nth-child(n+8)').slideToggle('');
            $(this).toggleClass('hide-more');
            if ($(this).hasClass('hide-more')) {
                $(this).html('Скрыть');

            } else {
                $(this).html('Показать еще');
            }
        })
    } else {
        $('#show-hide-button').hide();
    }



});
var swiper = new Swiper('.slider', {
    slidesPerView: 2,
    spaceBetween: 30,

});