$(document).ready(function () {
    $('#course-form').submit(function (e) {
        e.preventDefault();
        $.fancybox.close($('#write-course'));
        $.fancybox.open($('#thank-word'));
        return true;
    });
    $('#callback-form').submit(function (e) {
        e.preventDefault();
        $.fancybox.close($('#callback'));
        $.fancybox.open($('#thank-word'));
        return true;
    });
});