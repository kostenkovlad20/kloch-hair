(function () {
    'use strict';

    function tabNavClickHandler(e) {
      e.preventDefault();

      if (e.target.classList.contains('tab__link')) {
        $('.tab__link.active').removeClass('active');
        e.target.classList.add('active'); // $(`.tab__content[data-slide='${e.target}']`)

        $('.tab__content').removeClass('active');
        $(`[data-tab-id='${e.target.getAttribute('href')}']`).addClass('active');
      }
    }

    function bindEvents() {
      const tabNav = document.querySelector('.tab__nav');
      tabNav.addEventListener('click', tabNavClickHandler);
    }

    function init() {
      bindEvents();
    }

    var tab = {
      init: init
    };

    tab.init();
    $(".hamburger").click(function (e) {
      document.querySelector(".mobile").classList.add("active");
      document.querySelector("html").classList.add("oh");
      this.classList.add("active");
    });
    $(".layout").click(function (e) {
      document.querySelector(".mobile.active").classList.remove("active");
      document.querySelector("html").classList.remove("oh");
      document.querySelector(".hamburger").classList.remove("active");
    });
    $(".mobile__close").click(function (e) {
      document.querySelector(".mobile.active").classList.remove("active");
      document.querySelector("html").classList.remove("oh");
      document.querySelector(".hamburger").classList.remove("active");
    });

}());
