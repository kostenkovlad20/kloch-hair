const gulp = require("gulp");
const rollup = require("gulp-better-rollup");
const babel = require("rollup-plugin-babel");
const resolve = require("rollup-plugin-node-resolve");
const sass = require("gulp-sass");
const autoprefixer = require("gulp-autoprefixer");
const  csso = require('gulp-csso');
const sourcemaps = require('gulp-sourcemaps');

function scripts(done) {
  gulp
    .src("./src/js/index.*")
    .pipe(
      rollup(
        {
          plugins: [
            babel(),
            resolve({
              main: true,
              browser: true
            })
          ],
          onwarn: function(warning) {
            // Skip certain warnings

            // should intercept ... but doesn't in some rollup versions
            if (warning.code === "THIS_IS_UNDEFINED") {
              return;
            }

            // console.warn everything else
            console.warn(warning.message);
          }
        },
        {
          format: "iife"
        }
      )
    )
    .pipe(gulp.dest("./dist", { sourcemaps: true }));
  done();
}

function styles(done) {
  gulp
    .src("./src/styles/**/*")
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(csso())
    .pipe(autoprefixer({
      flexbox:true,
      add:true
    }))

    .pipe(gulp.dest("./dist", { sourcemaps: true }));
  done();
}

function watchFiles() {
  gulp.watch("./src/js/**/*", scripts);
  gulp.watch("./src/styles/**/*", styles);
}

exports.default = gulp.parallel(scripts, styles, watchFiles);
